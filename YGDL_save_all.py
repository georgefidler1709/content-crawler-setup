# Given a list of videos 'video_list', produces text-audio pairs
# and the appropriate metadata to be cleaned using kaldi methods.
# By George Fidler and Swahn Fraye - Language Confidence
from webvtt import WebVTT
from pydub import AudioSegment
from datetime import datetime
import uuid
import csv
import argparse
import os
import shutil
import subprocess
import logging
import sys
import pandas
import re
import string
from termcolor import cprint
from collections import OrderedDict
import tarfile
import boto3
import csv

s3 = boto3.resource('s3')
s3 = boto3.resource('s3',
         aws_access_key_id = "AKIAJKLCTRVKDOSTER5A",
         aws_secret_access_key = "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" )

BUCKET_NAME = 'audio-crawler'

glacier = boto3.client('glacier', 
         region_name='us-east-1',
         aws_access_key_id = "AKIAJKLCTRVKDOSTER5A",
         aws_secret_access_key = "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"
         )

VAULT_NAME = "crawler-archives"


# setup logger
logging.basicConfig(level=logging.INFO)
log = logging.getLogger('YT-dataminer')

def throws(msg):
  raise RuntimeError(msg)

# Clean up transcript
def cleanup_transcript(fileName): 
  transcript = WebVTT().read(fileName)
  # cleanup transcript text lines
  for line in transcript:
    line.text = line.text.strip()
    line.text = line.text.replace("\n", ' ')
    line.text = line.text.replace("  ", ' ')
    line.text = line.text.replace('"', '')
    line.text = line.text.replace('.' , '')
    line.text = line.text.replace(',' , '')
    line.text = line.text.replace('!' , '')
    line.text = line.text.replace('?' , '')
  return transcript

# # takes a transcript and selects a list of utterances to segment
def get_segmentation_list(transcript, videoID, videoTag, auto_trans, key_word):
  segList = OrderedDict()
  segList_stored = OrderedDict()
  #with open("lines.txt",'w') as linesFile:
  counter = 0
  #linesFile.write("VIDEOTAG =" + word)
  for i, line in enumerate(transcript):
    content = line.text
    if(content.find(' ') == -1):
      continue
    wordSplit = content.split()
    wordSplit = list(map(lambda x: x.lower(), wordSplit))
    if key_word in wordSplit or i == 42:
      segList, counter = add_to_segList(transcript, videoID, videoTag, auto_trans, content, counter, i, segList)
    else:
      segList_stored, counter = add_to_segList(transcript, videoID, videoTag, auto_trans, content, counter, i, segList_stored)
    #One word clips cause more problems than they are worth e.g. (*Laughter*)

  return (segList, segList_stored)

def add_to_segList(transcript, videoID, videoTag, auto_trans, content, counter, i, segList):
  if not auto_trans:
    segList[videoID + '-' + format(counter, '04d')] = {"start": transcript[i].start, "end": transcript[i].end, "utt": content }
    counter += 1
    #auto-generated transcripts have a very strange and disjointed format but this for some reason is how to get matching audio-text pairs
  elif (i % 2) == 1:
    segList[videoID + '-' + format(counter, '04d')] = {"start": transcript[i - 1].start, "end": transcript[i - 1].end, "utt": content }
    counter += 1
  return (segList, counter)
# HELPER returns time value in ms front string
def get_ms(time_str):
  h, m, s = time_str.split(':')
  return int(h) * 3600000 + int(m) * 60000 + float(s) * 1000


# takes a dictionary of audio to segment and sends
def segmentAndExportAudio(segList, audio, output_dir, videoTag):
  totalLengthExtracted = 0
  FLAC_FLAGS = "flac -c -d -s"
  WAV_PATH_ABSOLUTE = "/home/ubuntu/kaldi/egs/librispeech/s5/"
  with open("data/yt_dataset/wav.scp" , 'a') as wavDataFile, open("/dev/null" , 'w') as trash:
    for key in segList:
      # convert timetamps to ms
      startMs = get_ms(segList[key]['start'])
      endMs = get_ms(segList[key]['end'])
      # cut up audio segment
      segment = audio[startMs:endMs]
      totalLengthExtracted += segment.duration_seconds
      ID = key
      fileName = ID + '.flac'
      file_location = output_dir + '/' + videoTag + '/' + videoTag
      # Converts youtube audio from dual track to single track (as kaldi tools require)
      segment.export(file_location + '/' + 'stereo-' + fileName, format="flac")
      subprocess.call(["ffmpeg", "-i", file_location + '/' + 'stereo-' + fileName,  "-ac" , "1", file_location + '/' + fileName], stderr=trash)
      subprocess.call(["chmod" , "777" , file_location + '/' + fileName])
      os.remove(file_location + '/' + 'stereo-' + fileName)
      #audio clips saved for use
      wavDataFile.write(ID + ' ' + FLAC_FLAGS +' ' + WAV_PATH_ABSOLUTE + file_location + '/'+ fileName + ' |\n')
  return totalLengthExtracted

def segmentAudioForStorage(segList, audio, videoTag):
  with open("/dev/null" , 'w') as trash:
    for key in segList:
      # convert timetamps to ms
      startMs = get_ms(segList[key]['start'])
      endMs = get_ms(segList[key]['end'])
      # cut up audio segment
      segment = audio[startMs:endMs]
      ID = key
      fileName = ID + '.flac'
      file_location = 'tmp/audio-storage/' + videoTag
      # Converts youtube audio from dual track to single track (as kaldi tools require)
      segment.export(file_location + '/' + 'stereo-' + fileName, format="flac")
      subprocess.call(["ffmpeg", "-i", file_location + '/' + 'stereo-' + fileName,  "-ac" , "1", file_location + '/' + fileName], stderr=trash)
      subprocess.call(["chmod" , "777" , file_location + '/' + fileName])
      os.remove(file_location + '/' + 'stereo-' + fileName)

def storedToGlacier(videoTag, archiveDict):
  tar = tarfile.open(videoTag + '.tar','w:gz')
  try:
    for audioFile in os.listdir("tmp/audio-storage/" + videoTag):
      tar.addfile(tarfile.TarInfo(audioFile), open("tmp/audio-storage/" + videoTag + '/' + audioFile))
  finally:
    tar.close()
  with open(videoTag + '.tar', 'rb') as tarFile:
    response = glacier.upload_archive(
     archiveDescription= "audio from CC-crawler",
     body= tarFile,
     vaultName=VAULT_NAME
    )

  with open("archiveFile.txt", 'a') as archiveFile:
    print("writing " + videoTag + ' ' + response['archiveId'])
    archiveFile.write(videoTag + ' ' + response['archiveId'] + '\n')
    archiveFile.flush()
  os.remove(videoTag + '.tar')
  shutil.rmtree("tmp/audio-storage/" + videoTag)

def finalise_word_count_file(normFilePath, dict_type, tag):
  word_dict = dict()
  with open(normFilePath, 'r') as normFile:
    for line in normFile:
      line = line.strip('\n')
      line = line.split(' ')
      for single_word in line:
        if single_word in word_dict:
          word_dict[single_word] += 1
        else:
          word_dict[single_word] = 1

  with open("word_list.txt" , 'r') as wordFile:
    lines = wordFile.readlines()

  with open("tmp/word_counts/" + dict_type + "_" + tag + "_final.txt", 'w') as finalFile:
    for word in word_dict.keys():
      finalFile.write(word + ' ' + str(word_dict[word]) + '\n')
  subprocess.call(["sort" , '-k2', '-rn', '/home/ubuntu/kaldi/egs/librispeech/s5/tmp/word_counts/' + dict_type + "_" + tag + "_final.txt", "-o", '/home/ubuntu/kaldi/egs/librispeech/s5/tmp/word_counts/' + dict_type + "_" + tag + "_final.txt"])

def main():

  OUTPUT_DIR_PATH = "data/LibriSpeech/yt-dataset"
  NORM_DIR_PATH = "data/local/lm/librispeech-lm-corpus/corpus"
  archiveDict = dict()
  success_counter = 0
  try: 
    for file in os.listdir("tmp/url-files"):
      if not file.endswith(".txt"):
        continue
      VIDEO_LIST_PATH = "tmp/url-files/" + file
      word = file[:-4]
      urlCounter = 0
      with open('word_log.txt', 'r+') as logFile:
        used_words = logFile.read().strip().split()
        if word in used_words:
          continue
        else:
          logFile.write(word + '\n')

      # 2 loop through each video url in video list
      urls = [line.rstrip('\n') for line in open(VIDEO_LIST_PATH)]
      
      #ensures data from previous uses will not be matched with this data 
      fail_counter = 0
      for i, url in enumerate(urls):
        try:
          print("url " + str(i) + " for " + word)
          # download video and transcript
          cprint('====> Downloading Video', 'green')
          #NOTE: Right now only downloaded the hand written english transcript, if this does not exist the call will fail
          subprocess.call(["youtube-dl", "--sub-lang", 'en' , "--restrict-filenames", "--extract-audio", "--audio-format", "wav", "--write-sub","-o", "tmp/video.%(ext)s", url])
          # check if normal or auto-transcript
          if not os.path.exists('tmp/video.en.vtt'):
            subprocess.call(["youtube-dl", "--restrict-filenames", "--extract-audio", "--audio-format", "wav", "--write-auto-sub","-o", "tmp/video.%(ext)s", url])
            if not os.path.exists('tmp/video.en.vtt'):
              fail_counter += 1
              if fail_counter >= 10:
                with open('run_log.txt', 'a') as logFile:
                  logFile.write(word + '_failed' + '\n')
                break
                  #could have the server reset itself here
              continue
            else:
              with open('youtube-crawler/auto_transcripts', 'a') as myFile:
                myFile.write(url+"\n")
              auto_trans = True
          else: 
            auto_trans = False
          # clean up transcript
          transcript = cleanup_transcript('tmp/video.en.vtt')
        except:
          if os.path.isfile('tmp/video.wav'):
            os.remove('tmp/video.wav')
          if os.path.isfile('tmp/video.en.vtt'):
            os.remove('tmp/video.en.vtt')
          with open('run_log.txt', 'a') as logFile:
            logFile.write(word + ' - ' + videoTag + '- failed' + '\n')
          continue

        try:  
          # get list of sentences to extract
          videoTag = str(uuid.uuid4())
          videoTag = videoTag.replace('-' , '')
          videoID = videoTag + '-' + videoTag
          segList, segList_stored = get_segmentation_list(transcript, videoID, videoTag, auto_trans, word)
          #print("segList_stored = " + str(segList_stored))

          with open('run_log.txt', 'a') as logFile:
            logFile.write(word + ' - ' + videoTag + '\n')
          # if os.path.exists(OUTPUT_DIR_PATH + '/' + videoTag + '/' + videoTag):
          #   shutil.rmtree(OUTPUT_DIR_PATH + '/' + videoTag + '/' + videoTag)
          # if os.path.exists(NORM_DIR_PATH + '/' + videoTag):
          #   shutil.rmtree(NORM_DIR_PATH + '/' + videoTag)
          os.makedirs(OUTPUT_DIR_PATH + '/' + videoTag + '/' + videoTag)
          os.makedirs(NORM_DIR_PATH + '/' + videoTag)
          os.makedirs(NORM_DIR_PATH + '/stored-' + videoTag)
          os.makedirs('tmp/audio-storage/' + videoTag)
          # Kaldi scripts require each speaker have a gender.
          # With no good way to automatically discern speaker gender for now gender tag assigned randomly
          # to give an even spread.
          with open("data/yt_dataset/spk2gender" , 'a') as genderFile:
            genderFile.write(videoID + ' ')
            if(urlCounter % 2 == 0):
              genderFile.write('f')
            else:
              genderFile.write('m')
            genderFile.write('\n')

          #This depth of directories is expected by the kaldi scripts
          with open(NORM_DIR_PATH + '/' + videoTag + '/' + videoTag + '.txt' , "w", encoding='utf-8') as textFile, open('tmp/tags/' + videoTag + '.txt' , 'a') as tagFile:
          #with open(NORM_DIR_PATH + '/' + videoTag + '/' + videoTag + '.txt' , 'w') as textFile, open('tmp/tags/' + videoTag + '.txt' , 'w') as tagFile:
          #Text is saved to a file to be normalised while it's key is saved separately to be reunited later
          #So that the key is not normalised away  
            for key in segList:
              textFile.write(segList[key]['utt'] + ' .\n')
              tagFile.write(key + '\n')

          with open(NORM_DIR_PATH + '/stored-' + videoTag +'/stored-' + videoTag + '.txt' , "w", encoding='utf-8') as textFile, open('tmp/tags/stored/' + videoTag + '.txt' , 'a') as tagFile:
          #with open(NORM_DIR_PATH + '/' + videoTag + '/' + videoTag + '.txt' , 'w') as textFile, open('tmp/tags/' + videoTag + '.txt' , 'w') as tagFile:
          #Text is saved to a file to be normalised while it's key is saved separately to be reunited later
          #So that the key is not normalised away  
            for key in segList_stored:
              textFile.write(segList_stored[key]['utt'] + ' .\n')
              tagFile.write(key + '\n')

          #Weirdly the kaldi normalisation script expects something to this effect at the end of a transcript.
          #Likely because it is designed to work on books in project gutenburg which all seem to end this way.
          with open(NORM_DIR_PATH + '/' + videoTag + '/' + videoTag + '.txt' , 'a') as textFile:
             textFile.write('\n\n' + 'THE END' + '\n\n')
          with open(NORM_DIR_PATH + '/stored-' + videoTag + '/stored-' + videoTag + '.txt' , 'a') as textFile:
             textFile.write('\n\n' + 'THE END' + '\n\n')

          cprint('====> Segmenting audio from video', 'green')
          audio = AudioSegment.from_wav('tmp/video.wav') # import full audio
          segmentAndExportAudio(segList, audio, OUTPUT_DIR_PATH, videoTag)
          segmentAudioForStorage(segList_stored, audio, videoTag)

          storedToGlacier(videoTag, archiveDict)

          os.remove('tmp/video.wav')
          os.remove('tmp/video.en.vtt')
          urlCounter += 1
          success_counter += 1
        except:
          if os.path.exists(OUTPUT_DIR_PATH + '/' + videoTag):
            shutil.rmtree(OUTPUT_DIR_PATH + '/' + videoTag)
          if os.path.exists(NORM_DIR_PATH + '/' + videoTag):
            shutil.rmtree(NORM_DIR_PATH + '/' + videoTag)
          if os.path.exists(NORM_DIR_PATH + '/stored-' + videoTag):
            shutil.rmtree(NORM_DIR_PATH + '/stored-' + videoTag)
          if os.path.exists('tmp/audio-storage/' + videoTag):
            shutil.rmtree('tmp/audio-storage/' + videoTag)
          if os.path.exists("data/yt_dataset/spk2gender"):
            with open("data/yt_dataset/spk2gender", 'r') as inFile:
              lines = inFile.readlines()
            if videoID in lines[-1].split(' '):
              with open("data/yt_dataset/spk2gender",'w') as outFile:
                outFile.writelines([item for item in lines[:-1]])
          if os.path.isfile('tmp/tags/' + videoTag + '.txt'):
            os.remove('tmp/tags/' + videoTag + '.txt')
          if os.path.isfile('tmp/video.wav'):
            os.remove('tmp/video.wav')
          if os.path.isfile('tmp/video.en.vtt'):
            os.remove('tmp/video.en.vtt')
          continue
          # delete tmp files
    # write metadata file
    cprint('====> Writing meta files', 'green')

    if success_counter > 0:
    #Normalise the transcripts to Kaldi standards
      subprocess.call(["local/lm/train_lm.sh", "data/local/lm/librispeech-lm-corpus", "data/local/lm/norm/tmp",  "data/local/lm/norm/norm_texts" , "data/local/lm"])

      #Reunite keys with normalised text and save to appropriate locations
      with open("data/yt_dataset/text" , 'w') as totalDataFile, open("data/yt_dataset/utt2spk" , 'w') as uttFile:
        for file in os.listdir("tmp/tags"):
          if str(file) == 'stored':
            continue
          ID = file[:-4]
          with open('tmp/tags/' + ID +  '.txt' , 'r') as tagFile, open("data/local/lm/norm/norm_texts/" + ID + '/' + ID + '.txt', 'r') as normFile, open(OUTPUT_DIR_PATH + '/' + ID + '/' + ID + '/' + ID + '-' + ID + '.trans.txt' , 'a') as dataFile:

            for x, y in zip(tagFile, normFile):
              x = x.strip('\n')
              dataFile.write(x + ' ' + y)
              totalDataFile.write(x + ' ' + y)
              uttFile.write(x + ' ' +x[:-5] + '\n')
         # os.remove("data/local/lm/norm/norm_texts/" + ID + '/' + ID + '.txt')
      subprocess.call(["sort" , "/home/ubuntu/kaldi/egs/librispeech/s5/data/yt_dataset/utt2spk", "-o", "/home/ubuntu/kaldi/egs/librispeech/s5/data/yt_dataset/utt2spk"])
      subprocess.call(["sort" , "/home/ubuntu/kaldi/egs/librispeech/s5/data/yt_dataset/text", "-o", "/home/ubuntu/kaldi/egs/librispeech/s5/data/yt_dataset/text"])
      subprocess.call(["sort" , "/home/ubuntu/kaldi/egs/librispeech/s5/data/yt_dataset/wav.scp", "-o", "/home/ubuntu/kaldi/egs/librispeech/s5/data/yt_dataset/wav.scp"])
      subprocess.call(["sort" , "/home/ubuntu/kaldi/egs/librispeech/s5/data/yt_dataset/spk2gender", "-o", "/home/ubuntu/kaldi/egs/librispeech/s5/data/yt_dataset/spk2gender"])


      for file in os.listdir("tmp/tags/stored"):
        ID = file[:-4]
        try:
          with open('tmp/tags/stored/' + ID +  '.txt' , 'r') as tagFile, open("data/local/lm/norm/norm_texts/stored-" + ID + '/stored-' + ID + '.txt', 'r') as normFile, open('tmp/stored-utts/' + ID + '.txt' , 'a') as dataFile:
            for x, y in zip(tagFile, normFile):
              x = x.strip('\n')
              dataFile.write(x + ' ' + y)
        except:
          continue
        finalise_word_count_file("data/local/lm/norm/norm_texts/stored-" + ID + '/stored-' + ID + '.txt', "storage", ID)

      for file in os.listdir("tmp/word_counts"):
        if not file.startswith("storage") or not file.endswith(".txt"):
          continue
        s3.meta.client.upload_file('/home/ubuntu/kaldi/egs/librispeech/s5/tmp/word_counts/' + file, BUCKET_NAME, "US/word_counts/stored/" + file)

      with open('archiveFile.txt', 'r') as archiveFile:
        for line in archiveFile:
          line = line.strip('\n')
          line = line.split(' ')
          archiveDict[line[0]] = line[1]

      tag = str(uuid.uuid4())
      with open('stored-utts_' + tag + '.csv', 'a') as csvfile:
        filewriter = csv.writer(csvfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['videoID', 'uttID', 'content', 'archive-ID'])
        for file in os.listdir("tmp/stored-utts"):
          with open("tmp/stored-utts/" + file, 'r') as textFile:
            for line in textFile:
                line = line.strip('\n')
                line = line.replace('  ', ' ')
                breakPoint = line.find(' ')
                ID = line[:breakPoint]
                content = line[breakPoint:]
                ID = re.split('-', ID)
                videoID = ID[0]
                uttID = ID[2]
                archiveID = archiveDict[videoID]
                filewriter.writerow([videoID, uttID, content, archiveID])
        s3.meta.client.upload_file('/home/ubuntu/kaldi/egs/librispeech/s5/stored-utts_' + tag + '.csv', BUCKET_NAME, "US/stored-utts/stored-utts_" + tag + '.csv')

    else:
      with open('run_log.txt', 'a') as logFile:
        logFile.write("ERROR: no new utterances added\n")

    #if os.path.exists('tmp'):
    #  shutil.rmtree('tmp')
    return 0
  except Exception as e:
    log.exception(e)
    pass
  except SystemExit as e:
    # do clean up
    return 1
    
if __name__ == "__main__":
  sys.exit(main())
