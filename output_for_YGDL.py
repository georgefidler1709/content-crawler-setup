# Takes the cleaned result of the content-crawler (LC_run.sh) and creates
# the appropriate media data as output to export as a new speech dataset
# George Fidler - Language Confidence
import os
import re
import shutil
from pydub import AudioSegment
import boto3
import botocore
import csv
import uuid
import sys
import subprocess

BUCKET_NAME = 'audio-crawler'

s3 = boto3.resource('s3')
s3 = boto3.resource('s3',
         aws_access_key_id = "AKIAJKLCTRVKDOSTER5A",
         aws_secret_access_key = "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" )

def finalise_word_count_file(filePath, tag):
  word_dict = dict()
  with open(filePath, 'r') as file:
    for line in file:
      line = line.strip('\n')
      line = line.replace('<UNK>', '')
      line = line.replace('  ', ' ')
      line = line.split(' ')
      if line[0][-1] != '1':
        continue
      line = line[1:]
      for single_word in line:
        if single_word in word_dict:
          word_dict[single_word] += 1
        else:
          word_dict[single_word] = 1

  with open("word_list.txt" , 'r') as wordFile:
    lines = wordFile.readlines()

  with open("output/main_word_count_final-" + tag + ".txt", 'w') as finalFile:
    for word in word_dict.keys():
      finalFile.write(word + ' ' + str(word_dict[word]) + '\n')
  subprocess.call(["sort" , '-k2', '-rn', '/home/ubuntu/kaldi/egs/librispeech/s5/output/main_word_count_final-' + tag + '.txt', "-o", '/home/ubuntu/kaldi/egs/librispeech/s5/output/main_word_count_final-' + tag + '.txt'])


def main():
    # #create output file
    tag = str(uuid.uuid4())
    if os.path.exists('output'):
        shutil.rmtree('output')
    os.makedirs('output')
    with open("data/yt_dataset_cleaned/segments") as segFile:
        for line in segFile:
            #If there is a text-audio misalignment in the middle of a utterance
            #Kaldi will split it into 2 utterances e.g. 0000-0000-0058-1 and 0000-0000-0058-2
            #Handling these appropriately would result in more data but would require some work
            #As while kaldi writes them as separate time segments it does not separate the text.
            #i.e 2 separate audio segments referring to 1 utterance of text.
            #For now the second segment is ignored.
            #info we need is at the back of the string
            try:
                line = line.strip('\n')
                line = line.replace('  ', ' ')
                line = line.split(' ')
                if line[0][-1] != '1':
                    continue
                file = line[1].split('-')
                speakerID = file[0] 

                dataPath = speakerID + '/' + speakerID
                fullPath = 'data/LibriSpeech/yt-dataset/' + dataPath
                fileName = line[1] + '.flac'

                uttID = line[1][len(file[0] + '-' + file[1]):]

                #for some reason randomly sometimes timestamps start with -0
                line = [value for value in line if value != '']
                #in ms
                start = float(line[2]) * 1000
                end = float(line[3]) * 1000

                audio = AudioSegment.from_file(fullPath + '/' + fileName)
                #extract the audio segment
                extract = audio[start:end]
                exportFileName = speakerID + uttID + '.flac'
                extract.export('output/' + exportFileName, format="flac")
                s3.meta.client.upload_file('/home/ubuntu/kaldi/egs/librispeech/s5/output/' + exportFileName, BUCKET_NAME, "audio-cleaned/" + exportFileName,
                    ExtraArgs={
                        'StorageClass': 'STANDARD_IA'
                    })            
                os.remove('/home/ubuntu/kaldi/egs/librispeech/s5/output/' + exportFileName)
            except:
                continue
    #remove unknowns from the text (as their audio sections have been cut above) and moving to output
    with open('data/yt_dataset_cleaned/text', "r") as textFile, open('file_list_' + tag + '.csv', 'a') as csvfile:
        filewriter = csv.writer(csvfile, delimiter='\t', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['videoID', 'uttID', 'content'])
        for line in textFile:
            line = line.strip('\n')
            line = line.replace('<UNK>', '')
            line = line.replace('  ', ' ')
            if line.split(' ')[0][-1] != '1':
                continue
            breakPoint = line.find(' ')
            ID = line[:breakPoint]
            content = line[breakPoint:]
            ID = re.split('-', ID)
            videoID = ID[0]
            uttID = ID[2]
            filewriter.writerow([videoID, uttID, content])

    finalise_word_count_file('data/yt_dataset_cleaned/text', tag)
    s3.meta.client.upload_file('/home/ubuntu/kaldi/egs/librispeech/s5/output/main_word_count_final-' + tag + '.txt', BUCKET_NAME, "US/word_counts/main/main_word_count_final-" + tag + '.txt')  
    s3.meta.client.upload_file('/home/ubuntu/kaldi/egs/librispeech/s5/file_list_' + tag + '.csv', BUCKET_NAME, 'US/file_lists/file_list_' + tag + '.csv')
if __name__ == "__main__":
  sys.exit(main())
